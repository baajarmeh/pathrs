<?php


/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

use Carbon\Carbon;

$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    return [
        'first_name'            => $faker->firstName,
        'last_name'             => $faker->lastName,
        'phone'                 => $faker->phoneNumber,
        'email'                 => $faker->email,
        'password'              => $faker->password,
        'confirmation_token'    => $faker->randomNumber,
        'confirmed'             => true,
        'remember_token'        => $faker->randomNumber,
        'last_ip'               => "127.0.0.1",
        'last_login_date'       => Carbon::now()->subDays(2),
        'address'               => $faker->address,
        'api_token'             => $faker->randomNumber,
        'status'                => 1,
        'state'                 => $faker->stateAbbr,
        'remark'                => '',
        'created_by'            => factory(App\Models\User::class)->create()->id,
        'country_id'            => factory(App\Models\Country::class)->create()->id,
        'type'                  => 'user',
    ];
});
<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInitTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Create the user table
        Schema::create('users', function ($table) {
            $table->increments('id')->unsigned();
            $table->nullableTimestamps();
            $table->softDeletes();

            $table->string('first_name', 45)->nullable();
            $table->string('last_name', 45)->nullable();
            $table->string('phone', 45)->nullable();
            $table->string('email')->unique();
            $table->string('password');
            $table->string('confirmation_token')->nullable();
            $table->date('dob')->nullable();
            $table->string('address', 1000)->nullable();
            $table->string('passcode', 10)->nullable();
            $table->boolean('confirmed')->default(false);
            $table->string('remember_token', 100)->nullable();
            $table->string('last_ip')->nullable();
            $table->timestamp('last_login_date')->nullable();
            $table->string('api_token', 60)->unique()->nullable();
            $table->string('type', 45)->nullable();
            $table->text('remark')->nullable();
            $table->unsignedInteger('created_by')->nullable();
            
            $table->foreign('created_by')->references('id')->on('users')->onDelete('no action')->onUpdate('no action');
        });
        
        // Create the report table
        Schema::create('reports', function ($table) {
            $table->increments('id')->unsigned();
            $table->string('name');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->string('doctor_ref', 100)->nullable();
            $table->text('remarks')->nullable();
            $table->dateTime('result_at')->nullable();
            
            $table->unsignedInteger('created_by')->index();
            $table->foreign('created_by')->references('id')->on('users')->onDelete('no action')->onUpdate('no action');
            
            $table->unsignedInteger('patient_id')->index();
            $table->foreign('patient_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->softDeletes();
        });
        
        // Create the test table
        Schema::create('tests', function ($table) {
            $table->increments('id')->unsigned();
            $table->string('name', 100)->nullable()->index();
            $table->string('unit', 45)->nullable();
            $table->text('reference')->nullable();
            $table->text('remark')->nullable();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            
            $table->unsignedInteger('report_id')->index();
            $table->foreign('report_id')->references('id')->on('reports')->onDelete('no action')->onUpdate('no action');
            
            $table->unsignedInteger('created_by')->index();
            $table->foreign('created_by')->references('id')->on('users')->onDelete('no action')->onUpdate('no action');

            $table->softDeletes();
        });

        // Create the report result table
        Schema::create('report_results', function ($table) {
            $table->increments('id')->unsigned();
            $table->float('result', 8, 2)->nullable();
            $table->timestamp('created_at')->useCurrent();

            $table->unsignedInteger('report_id')->index();
            $table->foreign('report_id')->references('id')->on('reports')->onDelete('cascade')->onUpdate('cascade');

            $table->unsignedInteger('test_id')->index();
            $table->foreign('test_id')->references('id')->on('tests')->onDelete('cascade')->onUpdate('cascade');
            
            $table->unsignedInteger('created_by')->index();
            $table->foreign('created_by')->references('id')->on('users')->onDelete('no action')->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $tables = [
            'users',
            'reports',
            'tests',
            'report_results',
        ];

        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        foreach($tables as $table) {
            Schema::drop($table);
        }
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}

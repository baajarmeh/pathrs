<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Models\Report;

class ReportTest extends TestCase
{
    public function test_create_report_is_successful()
    {
        $this->actingAs($this->test_user)
            ->visit(route('showCreateReport'))
            ->type(1, 'patient_id')
            ->type($this->faker->name, 'name')
            ->type($this->faker->word, 'doctor_ref')
            ->type($this->faker->word, 'remarks')
            ->press('Save Report')
            ->seeJson([
                'status' => 'success'
            ]);
    }
}

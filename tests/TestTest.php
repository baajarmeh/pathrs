<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Models\Test;

class TestTest extends TestCase
{
    public function test_test_is_created_successfully()
    {
        $this->actingAs($this->test_user);

        $report = factory(App\Models\Report::class)->create(['created_by' => 1]);

        $server = array('HTTP_X-Requested-With' => 'XMLHttpRequest');

        $post = array(
            'report_id' => $report->id,
            'name' => $this->faker->text,
            'remark' => $this->faker->paragraph,
            'unit' => $this->faker->text,
            'reference' => $this->faker->paragraph
        );

        $this->call('post', route('postCreateTest', ['report_id' => $report->id]), $post, $server);

        $this->seeJson([
            'status' => 'success',
            'id' => 1,
        ]);
    }

    public function test_test_is_not_created_and_validation_error_messages_show()
    {
        $this->actingAs($this->test_user);

        $report = factory(App\Models\Report::class)->create(['created_by' => 1]);

        $server = array('HTTP_X-Requested-With' => 'XMLHttpRequest');

        $post = array(
            'report_id' => $report->id,
        );

        $this->call('post', route('postCreateTest', ['report_id' => $report->id]), $post, $server);

        $this->seeJson([
            'status' => 'error',
        ]);
    }
}

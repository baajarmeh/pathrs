<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Test extends Model
{
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string $table
     */
    protected $table = 'tests';

    /**
     * The result associated with the report result.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function result()
    {
        return $this->hasOne(\App\Models\ReportResult::class);
    }
    
    /**
     * The owner associated with the user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function owner()
    {
        return $this->belongsTo(\App\Models\User::class, 'created_by');
    }
    
    /**
     * The report associated with the report template.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function report()
    {
        return $this->belongsTo(\App\Models\Report::class);
    }
}

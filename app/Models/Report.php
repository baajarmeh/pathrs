<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    use SoftDeletes;

    /**
     * The validation rules.
     *
     * @var array $rules
     */
    protected $rules = [
        'name'        => ['required'],
        'doctor_ref'  => ['max:100'],
        'patient_id'  => ['required'],
    ];
            
    /**
     * The owner associated with the user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function owner()
    {
        return $this->belongsTo(\App\Models\User::class, 'created_by');
    }
    
    /**
     * The patient associated with the user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function patient()
    {
        return $this->belongsTo(\App\Models\User::class);
    }

    /**
     * The results associated with the report result.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function results()
    {
        return $this->hasMany(\App\Models\ReportResult::class);
    }
    
    /**
     * The tests associated with the test.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function tests()
    {
        return $this->hasManyThrough(\App\Models\Test::class, \App\Models\ReportResult::class);
    }
    
    /**
     * The attributes that should be mutated to dates.
     *
     * @return array $dates
     */
    public function getDates()
    {
        return ['created_at', 'updated_at', 'result_at'];
    }
}

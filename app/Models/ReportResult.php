<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Description of ReportResult.
 */
class ReportResult extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string $table
     */
    protected $table = 'report_results';
    
    /**
     * The owner associated with the user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function owner()
    {
        return $this->belongsTo(\App\Models\User::class, 'created_by');
    }
    
    /**
     * The owner associated with the user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function test()
    {
        return $this->belongsTo(\App\Models\Test::class);
    }
    
    /**
     * The patient associated with the user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function patient()
    {
        return $this->belongsTo(\App\Models\User::class);
    }
    
    /**
     * The report associated with the report.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function report()
    {
        return $this->belongsTo(\App\Models\Report::class);
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\User;
use Hash;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Mail;

class SignupController extends Controller
{
    protected $auth;

    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
        $this->middleware('guest');
    }

    public function showSignup()
    {
        return view('Public.LoginAndRegister.Signup');
    }

    /**
     * Creates an account.
     *
     * @param Request $request
     *
     * @return Redirect
     */
    public function postSignup(Request $request)
    {
        $this->validate($request, [
            'email'        => 'required|email|unique:users',
            'password'     => 'required|min:5|confirmed',
            'first_name'   => 'required',
            'terms_agreed' => 'required',
        ]);
        
        $user = new User();
        $user_data = $request->only(['email', 'first_name', 'last_name']);
        $user_data['password'] = Hash::make($request->get('password'));
        $user_data['type'] = 'user';
        $user = User::create($user_data);

        Mail::send('Emails.ConfirmEmail',
            ['first_name' => $user->first_name, 'confirmation_token' => $user->confirmation_token],
            function ($message) use ($request) {
                $message->to($request->get('email'), $request->get('first_name'))
                    ->subject('Thank you for registering for PLRS');
            });
        
        session()->flash('message', 'Success! You can now login.');

        return redirect('login');
    }

    /**
     * Confirm a user email
     *
     * @param $confirmation_token
     * @return mixed
     */
    public function confirmEmail($confirmation_token)
    {
        // $user = User::where('confirmation_token', $confirmation_code)->first();
        $user = User::whereConfirmationToken($confirmation_token)->first();

        if (!$user) {
            return view('Public.Errors.Generic', [
                'message' => 'The confirmation code is missing or malformed.',
            ]);
        }

        $user->confirmed = 1;
        $user->confirmation_token = null;
        $user->save();

        session()->flash('message', 'Success! Your email is now verified. You can now login.');

        return redirect()->route('login');
    }
}

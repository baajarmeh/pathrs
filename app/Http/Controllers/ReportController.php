<?php

namespace App\Http\Controllers;

use App\Models\Report;
use App\Models\User;
use Auth;
use Config;
use DB;
use Excel;
use Illuminate\Http\Request;
use Mail;
use Carbon;
use PDF;
use Validator;

class ReportController extends Controller
{
    /**
     * Generate and save the PDF result.
     *
     * @todo Move this from the order model
     *
     * @return bool
     */
    protected function _generatePdfResult($report)
    {
        $data = [
            'tests'     => $report->tests,
            'report'    => $report,
            'patient'   => $report->patient
        ];

        $pdf_file_path = public_path(config('app.patient_pdf_result_path')) . '/' . $this->report->patient->first_name .'-'. md5($this->report->patient->id);
        $pdf_file = $pdf_file_path . '.pdf';

        if (!is_dir($pdf_file_path)) {
            File::makeDirectory(dirname($pdf_file_path), 0777, true, true);
        }

        PDF::setOutputMode('F'); // force to file
        PDF::html('ManageTest.Partials.PDFResult', $data, $pdf_file_path);
        
        return file_exists($pdf_file) ? $pdf_file : false;
    }
     
    /**
     * Show the reports list
     *
     * @param Request $request
     * @return View
     */
    public function showIndex(Request $request)
    {
        $user_type = Auth::user()->type;
        
        $allowed_sorts = ['name', 'patient_name', 'status', 'result_at', 'doctor_ref'];

        $searchQuery = $request->get('q');
        $sort_order = $request->get('sort_order') == 'asc' ? 'asc' : 'desc';
        $sort_by = (in_array($request->get('sort_by'), $allowed_sorts) ? $request->get('sort_by') : 'created_at');

        if ($sort_by == 'patient_name') {
            $sort_by = 'patient.first_name';
        }
        
        $qry = Report::orderBy($sort_by, $sort_order);
        
        if ($user_type == 'patient') {
            $qry = $qry->where('patient_id', Auth::user()->id);
        }
        
        if ($searchQuery) {
            $qry = $qry->where('name', 'like', $searchQuery . '%')
                ->orWhere('patient.first_name', 'like', $searchQuery . '%')
                ->orWhere('patient.last_name', 'like', $searchQuery . '%')
                ->orWhere('doctor_ref', 'like', $searchQuery . '%');
        }
        
        $reports = $qry->paginate();

        $data = [
            'reports'    => $reports,
            'sort_by'    => $sort_by,
            'sort_order' => $sort_order,
            'q'          => $searchQuery ? $searchQuery : '',
        ];
        
        return view(($user_type == 'patient' ? 'ManageReport.Patient.Report' : 'ManageReport.Report'), $this->_filterData($request));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function showCreateReport(Request $request)
    {
        $patients = User::where('type', '=', 'patient')->pluck('first_name', 'id');

        return view('ManageReport.Modals.CreateReport', ['patients' => $patients]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @access public
     * @param  Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postCreateReport(Request $request)
    {
        $this->validate($request, [
            'name'        => ['required'],
            'doctor_ref'  => ['max:100'],
            'patient_id'  => ['required'],
        ]);
        
        // Create report.
        $report = new Report;

        $report->name = $request->get('name');
        $report->patient_id = $request->get('patient_id');
        $report->doctor_ref = $request->get('doctor_ref');
        // $report->result_at = $request->get('result_at') ? Carbon::createFromFormat('d-m-Y H:i', $request->get('result_at')) : null;
        $report->remarks = $request->get('remark');
        $report->created_by = Auth::user()->id;
        $report->save();

        session()->flash('message', 'Successfully Created Report');

        return response()->json([
            'status'      => 'success',
            'message'     => 'Refreshing..',
            'redirectUrl' => '',
        ]);
    }
    
    /**
     * Show the Edit Report modal
     *
     * @param Request $request
     * @param $report_id
     * @return View
     */
    public function showEditReport(Request $request, $report_id)
    {
        $report = Report::scope()->findOrFail($report_id);

        return view('ManageReport.Modals.EditReport', ['report' => $report]);
    }

    /**
     * Updates an report
     *
     * @param Request $request
     * @param $report_id
     * @return mixed
     */
    public function postEditReport(Request $request, $report_id)
    {
        $report = Report::scope()->findOrFail($report_id);
        
        $this->validate($request, [
            'name'        => ['required'],
            'doctor_ref'  => ['max:100'],
            'patient_id'  => ['required'],
        ]);

        $report->name = $request->get('name');
        $report->patient_id = $request->get('patient_id');
        $report->doctor_ref = $request->get('doctor_ref');
        // $report->result_at = $request->get('result_at') ? Carbon::createFromFormat('d-m-Y H:i', $request->get('result_at')) : null;
        $report->remarks = $request->get('remark');
        $report->save();

        session()->flash('message', 'Successfully Updated Report');

        return response()->json([
            'status'      => 'success',
            'id'          => $report->id,
            'redirectUrl' => '',
        ]);
    }

    /**
     * Delete a report
     *
     * @param Request $request
     * @param $report_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function postDeleteReport(Request $request, $report_id)
    {
        $report = Report::scope()->findOrFail($report_id);

        $report->results()->delete();

        if ($report->delete()) {
            session()->flash('message', 'Report Successfully Deleted');

            return response()->json([
                'status'      => 'success',
                'message'     => 'Refreshing..',
                'redirectUrl' => '',
            ]);
        }

        return response()->json([
            'status'  => 'error',
            'id'      => $report->id,
            'message' => 'This report can\'t be deleted.',
        ]);
    }
    
    /**
     * Send patient result to email
     *
     * @param Request $request
     * @param $report_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function postSendPatientResult(Request $request, $report_id)
    {
        $report = Report::findOrFial($report_id);

        if (Auth::user()->id != $report->patient_id && Auth::user()->type == 'patient')
        {
            return response()->json([
                'status'  => 'error',
                'id'      => $report->id,
                'message' => 'This report can\'t be download.',
            ]);
        }
        
        Log::info("Sending email to: " . $report->patient->email);
        
        $gen_file = $this->_generatePdfResult($report);
        
        if ($gen_file)
        {
            $data = [
                'report'  => $report,
                'patient' => $report->patient
            ];

            Mail::send('Mailers.SendPatientResult', $data, function ($message) use ($report) {
                $message->to($report->patient->email);
                $message->subject('Your result for the report ' . $report->template->name);

                $message->attach($gen_file);
            });
            
            return response()->json([
                'status'      => 'success',
                'message'     => 'Refreshing..',
                'redirectUrl' => '',
            ]);
        }
        
        return response()->json([
            'status'  => 'error',
            'id'      => $report->id,
            'message' => 'This report can\'t be sended.',
        ]);
    }
    
    /**
     * download a patient report
     *
     * @param Request $request
     * @param $report_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function postDownloadReport(Request $request, $report_id)
    {
        $report = Report::findOrFail($report_id);
        
        if (Auth::user()->id != $report->patient_id && Auth::user()->type == 'patient')
        {
            return response()->json([
                'status'  => 'error',
                'id'      => $report->id,
                'message' => 'This report can\'t be download.',
            ]);
        }

        $gen_file = $this->_generatePdfResult($report);

        if ($gen_file) {
            return response()->file($gen_file, ['Content-Type: application/pdf']);
        }

        return response()->json([
            'status'  => 'error',
            'id'      => $report->id,
            'message' => 'This report can\'t be download.',
        ]);
    }
}

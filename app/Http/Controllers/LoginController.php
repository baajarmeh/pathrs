<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Redirect;
use View;
use App\Models\User;

class LoginController extends Controller
{
    protected $auth;

    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
        $this->middleware('guest');
    }

    /**
     * Shows login form.
     *
     * @param Request $request
     *
     * @return mixed
     */
    public function showLogin(Request $request)
    {
        /*
         * If there's an ajax request to the login page assume the person has been
         * logged out and redirect them to the login page
         */
        if ($request->ajax()) {
            return response()->json([
                'status'      => 'success',
                'redirectUrl' => route('login'),
            ]);
        }

        return View::make('Public.LoginAndRegister.Login');
    }

    /**
     * Handles the login request.
     *
     * @param Request $request
     *
     * @return mixed
     */
    public function postLogin(Request $request)
    {
        $email = $request->get('email');
        $password = $request->get('password');

        if (empty($email) || empty($password)) {
            return Redirect::back()
                ->with(['message' => 'Please fill in your email and password', 'failed' => true])
                ->withInput();
        }

        if ($this->auth->attempt(['email' => $email, 'password' => $password], true) === false) {
            return Redirect::back()
                ->with(['message' => 'Your username/password combination was incorrect', 'failed' => true])
                ->withInput();
        }

        return redirect()->intended(route('login'));
    }

    /**
     * Shows patient login form.
     *
     * @param Request $request
     *
     * @return mixed
     */
    public function showPatientLogin(Request $request)
    {
        /*
         * If there's an ajax request to the login page assume the person has been
         * logged out and redirect them to the login page
         */
        if ($request->ajax()) {
            return response()->json([
                'status'      => 'success',
                'redirectUrl' => route('patientLogin'),
            ]);
        }

        return View::make('Public.LoginAndRegister.PatientLogin');
    }

    /**
     * Handles the login request.
     *
     * @param Request $request
     *
     * @return mixed
     */
    public function postPatientLogin(Request $request)
    {
        $email = $request->get('email');
        $passcode = $request->get('passcode');

        if (empty($email) || empty($passcode)) {
            return Redirect::back()
                ->with(['message' => 'Please fill in your email and passcode', 'failed' => true])
                ->withInput();
        }

        if ($this->auth->attempt(['email' => $email, 'passcode' => $passcode], true) === false) {
            return Redirect::back()
                ->with(['message' => 'Your username/passcode combination was incorrect', 'failed' => true])
                ->withInput();
        }
        
        return redirect()->intended(route('patientLogin'));
    }
}

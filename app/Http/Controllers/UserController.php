<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\ReportResult;
use Excel;
use Auth;
use Hash;
use Str;
use Illuminate\Http\Request;
use Validator;

class UserController extends Controller
{
    /**
     * filter data
     *
     * @param Request $request
     * @param String $type
     * @return mixed
     */
    protected function _filterData(Request $request, $type = 'patient')
    {
        $allowed_sorts = ['first_name', 'phone', 'passcode', 'last_login_date', 'created_by'];

        $searchQuery = $request->get('q');
        $sort_order = $request->get('sort_order') == 'asc' ? 'asc' : 'desc';
        $sort_by = (in_array($request->get('sort_by'), $allowed_sorts) ? $request->get('sort_by') : 'created_at');
        
        $filter = User::where('type', $type);
        
        if ($searchQuery)
        {
            $filter = $filter->andWhere('first_name', 'like', $searchQuery . '%')
                ->orWhere('last_name', 'like', $searchQuery . '%')
                ->orWhere('phone', 'like', $searchQuery . '%')
                ->orWhere('email', 'like', $searchQuery . '%');
        }
        
        $data = $filter->orderBy($sort_by == 'created_by' ? 'owner.first_name' : $sort_by, $sort_order)
                    ->paginate();

        return [
            Str::plural($type)  => $data,
            'sort_by'           => $sort_by,
            'sort_order'        => $sort_order,
            'q'                 => $searchQuery ? $searchQuery : '',
        ];
    }
    
    /**
     * Show the patient page
     *
     * @param Request $request
     * @return mixed
     */
    public function showPatients(Request $request)
    {
        return view('ManageUser.Patient', $this->_filterData($request, 'patient'));
    }

    /**
     * Show the user page
     *
     * @param Request $request
     * @return mixed
     */
    public function showUsers(Request $request)
    {
        return view('ManageUser.User', $this->_filterData($request, 'user'));
    }
    
    /**
     * Show the form for creating a new patient.
     *
     * @return \Illuminate\Http\Response
     */
    public function showCreatePatient(Request $request)
    {
        return view('ManageUser.Modals.CreatePatient');
    }

    /**
     * Store a newly created patient in storage.
     *
     * @access public
     * @param  Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postCreatePatient(Request $request)
    {
        $this->validate($request, [
            'email'        => 'required|email|unique:users',
            'passcode'     => 'required|min:5',
            'first_name'   => 'required',
            'phone'        => 'required',
        ]);

        // Create patient.
        $patient = new User;

        $patient->email = $request->get('email');
        $patient->passcode = $request->get('passcode');
        $patient->first_name = $request->get('first_name');
        $patient->last_name = $request->get('last_name');
        $patient->phone = $request->get('phone');
        $patient->address = $request->get('address');
        $patient->type = 'patient';
        $patient->confirmed = 1;
        $patient->password = Hash::make(str_random(10));
        $patient->created_by = Auth::user()->id;
        $patient->save();

        session()->flash('message', 'Successfully Created Patient');

        return response()->json([
            'status'      => 'success',
            'message'     => 'Refreshing..',
            'redirectUrl' => '',
        ]);
    }

    /**
     * Show the form for creating a new user.
     *
     * @return \Illuminate\Http\Response
     */
    public function showCreateUser(Request $request)
    {
        return view('ManageUser.Modals.CreateUser');
    }

    /**
     * Store a newly created user in storage.
     *
     * @access public
     * @param  Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postCreateUser(Request $request)
    {
        $this->validate($request, [
            'email'        => 'required|email|unique:users',
            'password'     => 'required|min:5|confirmed',
            'first_name'   => 'required'
        ]);

        // Create user.
        $user = new User;

        $user->email = $request->get('email');
        $user->password = Hash::make($request->get('password'));
        $user->first_name = $request->get('first_name');
        $user->last_name = $request->get('last_name');
        $user->phone = $request->get('phone');
        $user->address = $request->get('address');
        $user->type = 'user';
        $user->confirmed = 0;
        $user->created_by = Auth::user()->id;
        $user->save();

        Mail::send('Emails.ConfirmEmail',
            ['first_name' => $user->first_name, 'confirmation_token' => $user->confirmation_token],
            function ($message) use ($request) {
                $message->to($request->get('email'), $request->get('first_name'))
                    ->subject('Thank you for adding for PLRS');
            });
        
        session()->flash('message', 'Successfully Created User');

        return response()->json([
            'status'      => 'success',
            'message'     => 'Refreshing..',
            'redirectUrl' => '',
        ]);
    }
    
    /**
     * Show the Edit User or Patient Modal
     *
     * @param Request $request
     * @param $patient_id
     * @return mixed
     */
    public function showEditUser(Request $request, $user_id)
    {
        $user = User::scope()->findOrFail($user_id);

        $data = [
            'user'  => $user,
        ];

        return view('ManageUser.Modals.EditUser', $data);
    }
    
    /**
     * Edit a patient
     *
     * @param Request $request
     * @param $patient_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function postEditUser(Request $request, $user_id)
    {
        // Get the user | patient or display a 'not found' warning.
        $user = User::scope()->findOrFail($user_id);

        $this->validate($request, [
            'first_name'   => 'required',
            'phone'        => 'required',
        ]);

        // Edit user or patient.
        $user->first_name = $request->get('first_name');
        $user->last_name = $request->get('last_name');
        $user->phone = $request->get('phone');
        $user->address = $request->get('address');
        $user->save();

        session()->flash('message', 'Successfully Edited');

        return response()->json([
            'status'      => 'success',
            'message'     => 'Refreshing..',
            'redirectUrl' => '',
        ]);
    }

    /**
     * Delete a user or patient
     *
     * @param Request $request
     * @param $user_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function postDeleteUser(Request $request, $user_id)
    {
        $user = User::scope()->findOrFail($user_id);

        $user->results()->delete();
        
        if ($user->delete()) {
            session()->flash('message', 'User Successfully Deleted');

            return response()->json([
                'status'      => 'success',
                'message'     => 'Refreshing..',
                'redirectUrl' => '',
            ]);
        }

        return response()->json([
            'status'  => 'error',
            'id'      => $user->id,
            'message' => 'This user can\'t be deleted.',
        ]);
    }
    
    /**
     * Show all results to patient
     *
     * @param Request $request
     * @param $patient_id
     * @return mixed
     */
    public function showPatientResults(Request $request, $patient_id)
    {
        // Get the patient or display a 'not found' warning.
        $patient = User::scope()->findOrFail($patient_id);
        $results = ReportResult::scope()->where('patient_id', $patient_id)->get();

        $data = [
            'results'    => $results,
            'patient'    => $patient,
        ];

        return view('ManageUser.Modals.PatientResults', $data);
    }

    /**
     * Export patients to xls, csv etc.
     *
     * @param Request $request
     * @param string $export_as
     */
    public function showExportPatients(Request $request, $export_as = 'xlsx')
    {
        Excel::create('patients-as-of-' . date('d-m-Y-g.i.a'), function ($excel) {

            $excel->setTitle('Patients');

            // Chain the setters
            $excel->setCreator(config('app.name'))
                ->setCompany(config('app.name'));

            $excel->sheet('patients_sheet_', function ($sheet) {

                $patients = User::where('type', 'patient')->get();

                $sheet->fromArray($patients, null, 'A1', false, false);

                // Set gray background on first row
                $sheet->row(1, function ($row) {
                    $row->setBackground('#f5f5f5');
                });
            });
        })->export($export_as);
    }

    /**
     * Show the edit user modal
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function showEditProfile()
    {
        return view('ManageUser.Modals.EditProfile', ['user' => Auth::user()]);
    }

    /**
     * Updates the current user
     *
     * @param Request $request
     * @return mixed
     */
    public function postEditProfile(Request $request)
    {
        $rules = [
            'new_password' => ['min:5', 'confirmed', 'required_with:password'],
            'password'     => 'passcheck',
            'first_name'   => ['required'],
            'last_name'    => ['required'],
        ];

        $messages = [
            'password.passcheck'  => 'This password is incorrect.',
            'first_name.required' => 'Please enter your first name.',
            'last_name.required'  => 'Please enter your last name.',
        ];

        $validation = Validator::make($request->all(), $rules, $messages);

        if ($validation->fails()) {
            return response()->json([
                'status'   => 'error',
                'messages' => $validation->messages()->toArray(),
            ]);
        }

        $user = Auth::user();

        if ($request->get('password')) {
            $user->password = Hash::make($request->get('new_password'));
        }

        $user->first_name = $request->get('first_name');
        $user->last_name = $request->get('last_name');
        
        $user->save();

        return response()->json([
            'status'  => 'success',
            'message' => 'Successfully Saved Details',
        ]);
    }
}

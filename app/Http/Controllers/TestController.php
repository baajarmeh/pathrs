<?php

namespace App\Http\Controllers;

use App\Models\Test;
use App\Models\Report;
use App\Models\ReportResult;
use Excel;
use Auth;
use Carbon;
use Illuminate\Http\Request;

class TestController extends Controller
{    
    /**
     * Show the test page
     *
     * @param Request $request
     * @param $report_id
     * @return mixed
     */
    public function showIndex(Request $request, $report_id)
    {
        // Get the report or display a 'not found' warning.
        $report = Report::findOrFail($report_id);
        
        $allowed_sorts = ['name', 'report_name', 'delete'];

        $searchQuery = $request->get('q');
        $sort_order = $request->get('sort_order') == 'asc' ? 'asc' : 'desc';
        $sort_by = (in_array($request->get('sort_by'), $allowed_sorts) ? $request->get('sort_by') : 'created_at');

        if ($searchQuery) {
            $tests = Test::where('report_id', $report_id)
                ->andWhere('name', 'like', $searchQuery . '%')
                ->orWhere('report.name', 'like', $searchQuery . '%')
                ->orderBy($sort_by == 'report_name' ? 'report.name' : $sort_by, $sort_order)
                ->select('name, delete, created_at, report.name')
                ->paginate();
        } else {
            $tests = Test::where('report_id', $report_id)
                ->orderBy($sort_by == 'report_name' ? 'report.name' : $sort_by, $sort_order)
                ->select('name, delete, created_at, report.name')
                ->paginate();
        }

        $data = [
            'tests'      => $tests,
            'sort_by'    => $sort_by,
            'sort_order' => $sort_order,
            'q'          => $searchQuery ? $searchQuery : '',
            'report'     => $report
        ];

        return view('ManageTest.Test', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function showCreateTest(Request $request, $report_id)
    {
        // Get the report or display a 'not found' warning.
        $report = Report::findOrFail($report_id);
        
        return view('ManageTest.Modals.CreateTest', ['report' => $report]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @access public
     * @param  Request $request
     * @param $report_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function postCreateTest(Request $request, $report_id)
    {
        $this->validate($request, [
            'name'     => ['max:100'],
            'unit'     => ['max:45'],
        ]);
        
        // Create test.
        $test = new Test;

        $test->name = $request->get('name');
        $test->report_id = $report_id;
        $test->unit = $request->get('unit');
        $test->reference = $request->get('reference');
        $test->remark = $request->get('remark');
        $test->created_by = Auth::user()->id;
        $test->save();

        session()->flash('message', 'Successfully Created Test');

        return response()->json([
            'status'      => 'success',
            'message'     => 'Refreshing..',
            'redirectUrl' => '',
        ]);
    }

    /**
     * Show the Edit Test Modal
     *
     * @param Request $request
     * @param $report_id
     * @param $test_id
     * @return mixed
     */
    public function showEditTest(Request $request, $report_id, $test_id)
    {
        // Get the report or display a 'not found' warning.
        $report = Report::findOrFail($report_id);
        
        $test = Test::scope()->findOrFail($test_id);
        
        return view('ManageTest.Modals.EditTest', ['test' => $test, 'report' => $report]);
    }

    /**
     * Edit a test
     *
     * @param Request $request
     * @param $report_id
     * @param $test_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function postEditTest(Request $request, $report_id, $test_id)
    {
        // Get the report or display a 'not found' warning.
        $report = Report::findOrFail($report_id);
        
        $this->validate($request, [
            'name'     => ['max:100'],
            'unit'     => ['max:45'],
        ]);
        
        // Get the test or display a 'not found' warning.
        $test = Test::scope()->findOrFail($test_id);

        // Edit test.
        $test->name = $request->get('name');
        $test->unit = $request->get('unit');
        $test->reference = $request->get('reference');
        $test->remark = $request->get('remark');
        $test->save();

        session()->flash('message', 'Successfully Edited Test');

        return response()->json([
            'status'      => 'success',
            'message'     => 'Refreshing..',
            'redirectUrl' => '',
        ]);
    }

    /**
     * Delete a test
     *
     * @param Request $request
     * @param $report_id
     * @param $test_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function postDeleteTest(Request $request, $report_id, $test_id)
    {
        // Get the report or display a 'not found' warning.
        $report = Report::findOrFail($report_id);
        
        $test = Test::scope()->findOrFail($test_id);

        $test->results()->delete();

        if ($test->delete()) {
            session()->flash('message', 'Test Successfully Deleted');

            return response()->json([
                'status'      => 'success',
                'message'     => 'Refreshing..',
                'redirectUrl' => '',
            ]);
        }

        return response()->json([
            'status'  => 'error',
            'id'      => $test->id,
            'message' => 'This test can\'t be deleted.',
        ]);
    }

    /**
     * Show result for test
     *
     * @param Request $request
     * @param $report_id
     * @param $test_id
     * @return mixed
     */
    public function showTestResult(Request $request, $report_id, $test_id)
    {
        // Get the report or display a 'not found' warning.
        $report = Report::findOrFail($report_id);
        
        // Get the test or display a 'not found' warning.
        $test = Test::scope()->findOrFail($test_id);
        
        $result = ReportResult::where('test_id', $test_id)
            ->where('report_id', $report->id)
            ->where('patient_id', $report->patient->id)
            ->first();

        $data = [
            'result'  => $result,
            'test'    => $test,
            'report'  => $report
        ];

        return view('ManageTest.Modals.TestResult', $data);
    }

    /**
     * Edit result for test
     *
     * @param Request $request
     * @param $report_id
     * @param $test_id
     * @return mixed
     */
    public function postTestResult(Request $request, $report_id, $test_id)
    {
        // Get the report or display a 'not found' warning.
        $report = Report::findOrFail($report_id);
        
        // Get the test or display a 'not found' warning.
        $test = Test::scope()->findOrFail($test_id);
        
        $result = ReportResult::where('test_id', $test_id)
            ->where('report_id', $report->id)
            ->where('patient_id', $report->patient->id)
            ->first();
        
        // Edit result.
        $result->result = $request->get('result');
        $result->save();
        
        // Edit report.
        $report->result_at = Carbon::now();
        $report->save();

        session()->flash('message', 'Successfully Edited Result');

        return response()->json([
            'status'      => 'success',
            'message'     => 'Refreshing..',
            'redirectUrl' => '',
        ]);
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\User;
use Carbon\Carbon;
use DateInterval;
use DatePeriod;
use DateTime;

class DashboardController extends Controller
{
    /**
     * Show the user dashboard
     *
     * @param bool|false $event_id
     * @return \Illuminate\View\View
     */
    public function showDashboard()
    {
        return view('ManageReport.Dashboard');
    }

    /**
     * Show the patient dashboard
     *
     * @param bool|false $event_id
     * @return \Illuminate\View\View
     */
    public function showPatientDashboard()
    {
        return view('ManageReport.PatientDashboard');
    }
}

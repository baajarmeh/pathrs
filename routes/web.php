<?php

/*
 * Logout
 */
Route::any('/logout', [
    'uses' => 'LogoutController@doLogout',
    'as'   => 'logout',
]);

/*
 * Login
 */
Route::get('/login', [
    'as'   => 'login',
    'uses' => 'LoginController@showLogin',
]);
Route::post('/login', 'LoginController@postLogin');

/*
 * Patient Login
 */
Route::get('/patient-login', [
    'as'   => 'patientLogin',
    'uses' => 'LoginController@showPatientLogin',
]);
Route::post('/patient-login', 'LoginController@postPatientLogin');

/*
 * Forgot password
 */
Route::get('/forgot-password', [
    'as'   => 'forgotPassword',
    'uses' => 'PasswordResetController@getRemind',
]);
Route::post('/forgot-password', [
    'as'   => 'postForgotPassword',
    'uses' => 'PasswordResetController@postRemind',
]);

/*
 * Reset Password
 */
Route::get('/reset-password/{token}', [
    'as'   => 'showResetPassword',
    'uses' => 'PasswordResetController@getReset',
]);
Route::post('/reset-password', [
    'as'   => 'postResetPassword',
    'uses' => 'PasswordResetController@postReset',
]);

/*
 * Registration / Account creation
 */
Route::get('/signup', [
    'uses' => 'SignupController@showSignup',
    'as'   => 'showSignup',
]);
Route::post('/signup', 'SignupController@postSignup');

/*
 * Confirm Email
 */
Route::get('/signup/confirm_email/{confirmation_code}', [
    'as'   => 'confirmEmail',
    'uses' => 'SignupController@confirmEmail',
]);

/**
 *
 */
Route::group(['middleware' => ['patient'], 'prefix' => 'patient'], function() {
    /*
     * Dashboard
     */
    Route::get('/', [
        'as'   => 'showPatientDashboard',
        'uses' => 'DashboardController@showPatientDashboard',
    ]);
    
    Route::get('/reports', [
        'as'   => 'showPatientReports',
        'uses' => 'ReportController@showIndex',
    ]);
    
    Route::get('/download-report/{report_id}', [
        'as'   => 'postPatientDownloadReport',
        'uses' => 'ReportController@postDownloadReport',
    ]);
    
    Route::post('/send-email/{report_id}', [
        'as'   => 'postPatientEmailReport',
        'uses' => 'ReportController@postSendPatientResult'
    ]);
});

/*
 * Backend routes
 */
Route::group(['middleware' => ['auth']], function () {

    /*
     * Edit Profile
     */
    Route::group(['prefix' => 'profile'], function () {

        Route::get('/', [
            'as'   => 'showEditProfile',
            'uses' => 'UserController@showEditProfile',
        ]);
        Route::post('/', [
            'as'   => 'postEditProfile',
            'uses' => 'UserController@postEditProfile',
        ]);
    });

    /*
     * admin routes
     */
    Route::group(['prefix' => 'admin', 'middleware' => ['admin.only']], function () {
        /*
         * Dashboard
         */
        Route::get('/dashboard', [
            'as'   => 'showDashboard',
            'uses' => 'DashboardController@showDashboard',
        ]);

         /*
         * Patient section
         */
        Route::group(['prefix' => 'patients'], function () {
            Route::get('/', [
                'as'   => 'showPatients',
                'uses' => 'UserController@showPatients',
            ]);

            Route::get('/create', [
                'as'   => 'showCreatePatient',
                'uses' => 'UserController@showCreatePatient'
            ]);
            Route::post('/create', [
                'as'   => 'postCreatePatient',
                'uses' => 'UserController@postCreatePatient'
            ]);

            Route::get('/export/{export_as?}', [
                'as'   => 'showExportPatients',
                'uses' => 'UserController@showExportPatients',
            ]);

            Route::get('/{patient_id}/results', [
                'as'   => 'showPatientResults',
                'uses' => 'UserController@showPatientResults',
            ]);
        });

         /*
         * User section
         */
        Route::group(['prefix' => 'users'], function () {
            Route::get('/', [
                'as'   => 'showUsers',
                'uses' => 'UserController@showUsers',
            ]);

            Route::get('/create', [
                'as'   => 'showCreateUser',
                'uses' => 'UserController@showCreateUser'
            ]);
            Route::post('/create', [
                'as'   => 'postCreateUser',
                'uses' => 'UserController@postCreateUser'
            ]);

            Route::get('/{user_id}', [
                'as'   => 'showEditUser',
                'uses' => 'UserController@showEditUser'
            ]);
            Route::post('/{user_id}', [
                'as'   => 'postEditUser',
                'uses' => 'UserController@postEditUser'
            ]);

            Route::post('/delete/{user_id}', [
                'as'   => 'postDeleteUser',
                'uses' => 'UserController@postDeleteUser'
            ]);
        });

        /*
         * Report
         */
        Route::group(['prefix' => 'reports'], function () {
            Route::get('/', [
                'as'   => 'showReports',
                'uses' => 'ReportController@showIndex',
            ]);

            Route::get('/create', [
                'as'   => 'showCreateReport',
                'uses' => 'ReportController@showCreateReport'
            ]);
            Route::post('/create', [
                'as'   => 'postCreateReport',
                'uses' => 'ReportController@postCreateReport'
            ]);

            Route::get('/{report_id}', [
                'as'   => 'showEditReport',
                'uses' => 'ReportController@showEditReport'
            ]);
            Route::post('/{report_id}', [
                'as'   => 'postEditReport',
                'uses' => 'ReportController@postEditReport'
            ]);

            Route::post('/delete/{report_id}', [
                'as'   => 'postDeleteReport',
                'uses' => 'ReportController@postDeleteReport'
            ]);
            
            Route::post('/download/{report_id}', [
                'as'   => 'postDownloadReport',
                'uses' => 'ReportController@postDownloadReport'
            ]);
            
            Route::post('/send-email/{report_id}', [
                'as'   => 'postEmailReport',
                'uses' => 'ReportController@postSendPatientResult'
            ]);
            
            /*
            * Test
            */
            Route::get('/{report_id}/tests', [
                'as'   => 'showTests',
                'uses' => 'TestController@showIndex',
            ]);

            Route::get('/{report_id}/tests/create', [
                'as'   => 'showCreateTest',
                'uses' => 'TestController@showCreateTest'
            ]);
            Route::post('/{report_id}/tests/create', [
                'as'   => 'postCreateTest',
                'uses' => 'TestController@postCreateTest'
            ]);

            Route::get('/{report_id}/tests/edit/{test_id}', [
                'as'   => 'showEditTest',
                'uses' => 'TestController@showEditTest'
            ]);
            Route::post('/{report_id}/tests/edit/{test_id}', [
                'as'   => 'postEditTest',
                'uses' => 'TestController@postEditTest'
            ]);

            Route::post('/{report_id}/tests/delete/{test_id}', [
                'as'   => 'postDeleteTest',
                'uses' => 'TestController@postDeleteTest'
            ]);

            Route::get('/{report_id}/tests/export/{export_as?}', [
                'as'   => 'showExportTests',
                'uses' => 'TestController@showExportTests',
            ]);

            Route::get('/{report_id}/tests/results/{test_id}', [
                'as'   => 'showTestResult',
                'uses' => 'TestController@showTestResult',
            ]);
            Route::post('/{report_id}/tests/results/{test_id}', [
                'as'   => 'postTestResult',
                'uses' => 'TestController@postTestResult',
            ]);
        });
    });
});

Route::get('/', function () {
    return Redirect::route('login');
});

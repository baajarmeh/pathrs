@extends('Shared.Layouts.MasterWithoutMenus')

@section('title', 'Patient Login')

@section('content')
    {!! Form::open(['url' => 'patient-login']) !!}
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="panel">
                <div class="panel-body">

                    @if(Session::has('failed'))
                        <h4 class="text-danger mt0">Whoops! </h4>
                        <ul class="list-group">
                            <li class="list-group-item">Please check your details and try again.</li>
                        </ul>
                    @endif

                    <div class="form-group">
                        {!! Form::label('email', 'Email', ['class' => 'control-label']) !!}
                        {!! Form::text('email', null, ['class' => 'form-control', 'autofocus' => true]) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('passcode', 'PassCode', ['class' => 'control-label']) !!}
                        {!! Form::password('passcode',  ['class' => 'form-control']) !!}
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-block btn-success">Login</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
@stop

@extends('Shared.Layouts.Master')

@section('title')
    @parent Test Admin
@stop

@section('top_nav')
@stop

@section('page_title')
    <i class='ico-clipboard4 mr5'></i>
    Tests
@stop

@section('head')
@stop

@section('page_header')
    <div class="col-md-9 col-sm-6">
        <!-- Toolbar -->
        <div class="btn-toolbar" role="toolbar">
            <div class="btn-group btn-group btn-group-responsive">

                <button class="loadModal btn btn-success" type="button" data-modal-id="CreateTest"
                        href="javascript:void(0);"
                        data-href="{{route('showCreateTest', ['report_id' => $report->id])}}">
                    <i class="ico-question"></i> Add Test
                </button>
            </div>
        </div>
        <!--/ Toolbar -->
    </div>
    <div class="col-md-3 col-sm-6">
      {!! Form::open(array('url' => route('showTests', ['report_id' => $report->id, 'sort_by' => $sort_by]), 'method' => 'get')) !!}
        <div class="input-group">
            <input name="q" value="{{$q or ''}}" placeholder="Search Tests.." type="text" class="form-control" />
            <span class="input-group-btn">
                <button class="btn btn-default" type="submit"><i class="ico-search"></i></button>
            </span>
        </div>
      {!! Form::close() !!}
    </div>
@stop

@section('content')
    <!--Start Tests table-->
    <div class="row">
        @if($tests->count())
            <div class="col-md-12">

                <!-- START panel -->
                <div class="panel">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <th>
                               Name
                            </th>
                            <th>
                               Report
                            </th>
                            <th>
                               Create at
                            </th>
                            <th></th>
                            </thead>

                            <tbody class="sortable">
                            @foreach ($tests as $test)
                                <tr id="test-{{ $test->id }}" data-test-id="{{ $test->id }}" class="{{$test->delete ? 'danger' : ''}}">
                                    <td>{{$test->name}}</td>
                                    <td>{{$test->report->name}}</td>
                                    <td>{{$test->created_at->format('d/m/Y H:i')}}</td>
                                    <td class="text-center">
                                        <a class="btn btn-xs btn-primary loadModal" data-modal-id="EditTest"
                                           href="javascript:void(0);"
                                           data-href="{{route('showEditTest', ['report_id' => $report->id, 'test_id' => $test->id])}}">
                                            Edit
                                        </a>
                                        <a class="btn btn-xs btn-primary loadModal" href="javascript:void(0);"
                                           data-href="{{route('showTestResult', ['report_id' => $report->id, 'test_id' => $test->id])}}">
                                            Result
                                        </a>
                                        <a data-id="{{ $test->id }}"
                                           title="All report results will also be deleted."
                                           data-route="{{route('postDeleteTest', ['report_id' => $report->id, 'test_id' => $test->id])}}"
                                           data-type="question" href="javascript:void(0);"
                                           class="deleteThis btn btn-xs btn-danger">
                                            Delete
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        @else
            @include('ManageTest.Partials.TestBlankSlate')
        @endif
    </div>    <!--/End tests table-->
@stop

<div id="TestForm" role="dialog" class="modal fade" style="display: none;">
    {!! Form::open(['url' => route('postCreateTest', ['report_id' => $report->id]), 'id' => 'create-test-form', 'class' => 'ajax']) !!}

    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header text-center">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title">
                    <i class="ico-question"></i>
                    Create Test
                </h3>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    {!! Form::label('name', 'Name', ['class' => 'control-label required']) !!}
                    {!! Form::text('name', '', [
                        'id' => 'test-name',
                        'class' => 'form-control',
                        'placeholder' => 'e.g. Please enter your test name?',
                    ]) !!}
                </div>

                <div class="form-group">
                  {!! Form::label('unit', 'Unit', ['class' => 'control-label required']) !!}
                  {!! Form::text('unit', Input::old('unit'), ['class' => 'form-control']) !!}
                </div>

                <div class="form-group">
                  {!! Form::label('reference', 'Reference', ['class' => 'control-label']) !!}
                  {!! Form::textarea('reference', Input::old('reference'), ['class' => 'form-control editable', 'rows' => 5]) !!}
                </div>

                <div class="form-group">
                  {!! Form::label('remark', 'Remark', ['class' => 'control-label']) !!}
                  {!! Form::textarea('remark', Input::old('remark'), ['class' => 'form-control editable', 'rows' => 5]) !!}
                </div>

            </div> <!-- /end modal body-->
            <div class="modal-footer">
                {!! Form::button('Cancel', ['class' => "btn modal-close btn-danger", 'data-dismiss' => 'modal']) !!}
                {!! Form::submit('Save Test', ['class' => "btn btn-success"]) !!}
            </div>
        </div><!-- /end modal content-->
    </div>
    {!! Form::close() !!}
</div>

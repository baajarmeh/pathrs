<div id="TestResultForm" role="dialog" class="modal fade" style="display: none;">
    {!! Form::model($test, ['url' => route('postTestResult', ['report_id' => $report->id, 'test_id' => $test->id]), 'id' => 'test-result-form', 'class' => 'ajax']) !!}

    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header text-center">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title">
                    <i class="ico-question"></i>
                    Test Result
                </h3>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    {!! Form::label('report_id', 'Report', ['class' => 'control-label']) !!}
                    {!! Form::label('report_name', $report->name, ['class' => 'control-label']) !!}
                </div>
                
                <div class="form-group">
                    {!! Form::label('patient_id', 'Patient', ['class' => 'control-label']) !!}
                    {!! Form::label('patient_name', ($report->patient->first_name .' '. $report->patient->last_name), ['class' => 'control-label']) !!}
                </div>
                
                <div class="form-group">
                    {!! Form::label('test_id', 'Test', ['class' => 'control-label']) !!}
                    {!! Form::label('test_name', $test->name, ['class' => 'control-label']) !!}
                </div>

                <div class="form-group">
                  {!! Form::label('result', 'Result', ['class' => 'control-label required']) !!}
                  {!! Form::text('result', ($result->result ? $result->result : ''), ['class' => 'form-control']) !!}
                </div>
            </div> <!-- /end modal body-->
            <div class="modal-footer">
                {!! Form::button('Cancel', ['class' => "btn modal-close btn-danger", 'data-dismiss' => 'modal']) !!}
                {!! Form::submit('Save Result', ['class' => "btn btn-success"]) !!}
            </div>
        </div><!-- /end modal content-->
    </div>
    {!! Form::close() !!}
</div>

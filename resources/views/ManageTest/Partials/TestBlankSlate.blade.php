@extends('Shared.Layouts.BlankSlate')

@section('blankslate-icon-class')
    ico-question2
@stop

@section('blankslate-title')
    No Tests Yet
@stop

@section('blankslate-text')
  New tests will appear here as they are created.
@stop

@section('blankslate-body')
    <button data-invoke="modal" data-modal-id='CreateTest' data-href="{{route('showCreateTest', ['report_id' => $report->id])}}" href='javascript:void(0);'  class=' btn btn-success mt5 btn-lg' type="button" >
        <i class="ico-question"></i>
        Create Test
    </button>
@stop

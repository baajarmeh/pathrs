@extends('Shared.Layouts.Master')

@section('title')
    @parent User Admin
@stop

@section('top_nav')
@stop

@section('page_title')
    <i class='ico-clipboard4 mr5'></i>
    Users
@stop

@section('head')
@stop

@section('page_header')
    <div class="col-md-9 col-sm-6">
        <!-- Toolbar -->
        <div class="btn-toolbar" role="toolbar">
            <div class="btn-group btn-group btn-group-responsive">

                <button class="loadModal btn btn-success" type="button" data-modal-id="CreateUser"
                        href="javascript:void(0);"
                        data-href="{{route('showCreateUser')}}">
                    <i class="ico-question"></i> Add User
                </button>
            </div>
        </div>
        <!--/ Toolbar -->
    </div>
    <div class="col-md-3 col-sm-6">
      {!! Form::open(array('url' => route('showUsers', ['sort_by' => $sort_by]), 'method' => 'get')) !!}
        <div class="input-group">
            <input name="q" value="{{$q or ''}}" placeholder="Search Users.." type="text" class="form-control" />
            <span class="input-group-btn">
                <button class="btn btn-default" type="submit"><i class="ico-search"></i></button>
            </span>
        </div>
      {!! Form::close() !!}
    </div>
@stop

@section('content')
    <!--Start Users table-->
    <div class="row">
        @if($users->count())
            <div class="col-md-12">

                <!-- START panel -->
                <div class="panel">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <th>
                               Name
                            </th>
                            <th>
                               Phone
                            </th>
                            <th>
                               Create at
                            </th>
                            <th>
                               Created by
                            </th>
                            <th>
                               Last Login
                            </th>
                            <th></th>
                            </thead>

                            <tbody class="sortable">
                            @foreach ($users as $user)
                                <tr id="user-{{ $user->id }}" data-user-id="{{ $user->id }}" class="{{$user->delete ? 'danger' : ''}}">
                                    <td>{{$user->first_name}} {{$user->last_name}}</td>
                                    <td>{{$user->phone}}</td>
                                    <td>{{$user->created_at->format('d/m/Y H:i')}}</td>
                                    <td>{{$user->owner->first_name}} {{$user->owner->last_name}}</td>
                                    <td>{{$user->last_login_date ? $user->last_login_date->format('d/m/Y H:i') : ''}}</td>
                                    <td class="text-center">
                                        <a class="btn btn-xs btn-primary loadModal" data-modal-id="EditUser"
                                           href="javascript:void(0);"
                                           data-href="{{route('showEditUser', ['user_id' => $user->id])}}">
                                            Edit
                                        </a>
                                        <a data-id="{{ $user->id }}"
                                           title="All report results will also be deleted."
                                           data-route="{{ route('postDeleteUser', ['user_id' => $user->id]) }}"
                                           data-type="question" href="javascript:void(0);"
                                           class="deleteThis btn btn-xs btn-danger">
                                            Delete
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        @else
            @include('ManageUser.Partials.UserBlankSlate')
        @endif
    </div>    <!--/End users table-->
@stop

@extends('Shared.Layouts.BlankSlate')

@section('blankslate-icon-class')
    ico-question2
@stop

@section('blankslate-title')
    No Patients Yet
@stop

@section('blankslate-text')
  New patient will appear here as they are created.
@stop

@section('blankslate-body')
    <button data-invoke="modal" data-modal-id='CreatePatient' data-href="{{route('showCreatePatient')}}" href='javascript:void(0);' class=' btn btn-success mt5 btn-lg' type="button" >
        <i class="ico-question"></i>
        Create Patient
    </button>
@stop

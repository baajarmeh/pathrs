@extends('Shared.Layouts.BlankSlate')

@section('blankslate-icon-class')
    ico-question2
@stop

@section('blankslate-title')
    No Users Yet
@stop

@section('blankslate-text')
  New user will appear here as they are created.
@stop

@section('blankslate-body')
    <button data-invoke="modal" data-modal-id='CreateUser' data-href="{{route('showCreateUser')}}" href='javascript:void(0);' class=' btn btn-success mt5 btn-lg' type="button" >
        <i class="ico-question"></i>
        Create User
    </button>
@stop

@extends('Shared.Layouts.Master')

@section('title')
    @parent Patient Admin
@stop

@section('top_nav')
@stop

@section('page_title')
    <i class='ico-clipboard4 mr5'></i>
    Patients
@stop

@section('head')
@stop

@section('page_header')
    <div class="col-md-9 col-sm-6">
        <!-- Toolbar -->
        <div class="btn-toolbar" role="toolbar">
            <div class="btn-group btn-group btn-group-responsive">

                <button class="loadModal btn btn-success" type="button" data-modal-id="CreatePatient"
                        href="javascript:void(0);"
                        data-href="{{route('showCreatePatient')}}">
                    <i class="ico-question"></i> Add Patient
                </button>
            </div>
        </div>
        <!--/ Toolbar -->
    </div>
    <div class="col-md-3 col-sm-6">
      {!! Form::open(array('url' => route('showPatients', ['sort_by' => $sort_by]), 'method' => 'get')) !!}
        <div class="input-group">
            <input name="q" value="{{$q or ''}}" placeholder="Search Patients.." type="text" class="form-control" />
            <span class="input-group-btn">
                <button class="btn btn-default" type="submit"><i class="ico-search"></i></button>
            </span>
        </div>
      {!! Form::close() !!}
    </div>
@stop

@section('content')
    <!--Start Patients table-->
    <div class="row">
        @if($patients->count())
            <div class="col-md-12">

                <!-- START panel -->
                <div class="panel">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <th>
                               Name
                            </th>
                            <th>
                               Phone
                            </th>
                            <th>
                               PassCode
                            </th>
                            <th>
                               Created by
                            </th>
                            <th>
                               Last Login
                            </th>
                            <th></th>
                            </thead>

                            <tbody class="sortable">
                            @foreach ($patients as $patient)
                                <tr id="patient-{{ $patient->id }}" data-patient-id="{{ $patient->id }}" class="{{$patient->delete ? 'danger' : ''}}">
                                    <td>{{$patient->first_name}} {{$patient->last_name}}</td>
                                    <td>{{$patient->phone}}</td>
                                    <td>{{$patient->passcode}}</td>
                                    <td>{{$patient->owner->first_name}} {{$patient->owner->last_name}}</td>
                                    <td>{{$patient->last_login_date ? $patient->last_login_date->format('d/m/Y H:i') : ''}}</td>
                                    <td class="text-center">
                                        <a class="btn btn-xs btn-primary loadModal" data-modal-id="EditPatient"
                                           href="javascript:void(0);"
                                           data-href="{{route('showEditPatient', ['patient_id' => $patient->id])}}">
                                            Edit
                                        </a>
                                        <a class="btn btn-xs btn-primary loadModal" href="javascript:void(0);"
                                           data-href="{{route('showPatientDownload', ['patient_id' => $patient->id])}}">
                                            Download Report
                                        </a>
                                        <a data-id="{{ $patient->id }}"
                                           title="Send report to patient email direct."
                                           data-route="{{ route('postPatientEmail', ['patient_id' => $patient->id]) }}"
                                           data-type="question" href="javascript:void(0);"
                                           class="btn btn-xs btn-success">
                                            Send Email
                                        </a>
                                        <a data-id="{{ $patient->id }}"
                                           title="All report results will also be deleted."
                                           data-route="{{ route('postDeleteUser', ['user_id' => $patient->id]) }}"
                                           data-type="question" href="javascript:void(0);"
                                           class="deleteThis btn btn-xs btn-danger">
                                            Delete
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        @else
            @include('ManageUser.Partials.PatientBlankSlate')
        @endif
    </div>    <!--/End patients table-->
@stop

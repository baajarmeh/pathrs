<div id="PatientForm" role="dialog" class="modal fade" style="display: none;">
    {!! Form::model($patient, ['url' => route('postEditPatient', ['patient_id' => $patient->id]), 'id' => 'edit-patient-form', 'class' => 'ajax']) !!}

    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header text-center">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title">
                    <i class="ico-question"></i>
                    Edit Patient
                </h3>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                          {!! Form::label('first_name', 'First Name', ['class' => 'control-label required']) !!}
                          {!! Form::text('first_name', $patient->first_name, ['class' => 'form-control']) !!}
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                          {!! Form::label('last_name', 'Last Name', ['class' => 'control-label required']) !!}
                          {!! Form::text('last_name', $patient->last_name, ['class' => 'form-control']) !!}
                      </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                          {!! Form::label('phone', 'Phone', ['class' => 'control-label required']) !!}
                          {!! Form::text('phone', $patient->phone, ['class' => 'form-control']) !!}
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                          {!! Form::label('address', 'Address', ['class' => 'control-label']) !!}
                          {!! Form::textarea('address', $patient->address, ['class' => 'form-control editable', 'rows' => 5]) !!}
                      </div>
                    </div>
                </div>

            </div> <!-- /end modal body-->
            <div class="modal-footer">
                {!! Form::button('Cancel', ['class' => "btn modal-close btn-danger", 'data-dismiss' => 'modal']) !!}
                {!! Form::submit('Save Patient', ['class' => "btn btn-success"]) !!}
            </div>
        </div><!-- /end modal content-->
    </div>
    {!! Form::close() !!}
</div>

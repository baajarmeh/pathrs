<div role="dialog" class="modal fade" style="display: none;">
    <style>
        .account_settings .modal-body {
            border: 0;
            margin-bottom: -35px;
            border: 0;
            padding: 0;
        }
        .account_settings .panel-footer {
            margin: -15px;
            margin-top: 20px;
        }
        .account_settings .panel {
            margin-bottom: 0;
            border: 0;
        }
    </style>
    <div class="modal-dialog account_settings">
        <div class="modal-content">
            <div class="modal-header text-center">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title">
                    <i class="ico-cogs"></i>
                    My Profile
                </h3>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <!-- tab -->
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#general_account" data-toggle="tab">General</a></li>
                        </ul>
                        <div class="tab-content panel">
                            <div class="tab-pane active" id="general_account">
                                @if(!Auth::user()->first_name)
                                    <div class="alert alert-info">
                                        <b>
                                            Welcome to {{config('app.name')}}!
                                        </b>
                                        <br>
                                        Before you continue please update your account with your name and a new password.
                                    </div>
                                @endif
                                
                                {!! Form::model($user, ['url' => route('postEditProfile'), 'class' => 'ajax closeModalAfter']) !!}
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            {!! Form::label('first_name', 'First Name', ['class' => 'control-label required']) !!}
                                            {!! Form::text('first_name', Input::old('first_name'), ['class' => 'form-control']) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            {!! Form::label('last_name', 'Last Name', ['class' => 'control-label required']) !!}
                                            {!! Form::text('last_name', Input::old('last_name'), ['class' => 'form-control']) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            {!! Form::label('email', 'Email', ['class' => 'control-label required']) !!}
                                            {!! Form::text('email', Input::old('email'), ['class' => 'form-control']) !!}
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row more-options">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            {!! Form::label('password', 'Old Password', ['class' => 'control-label']) !!}
                                            {!! Form::password('password', ['class' => 'form-control']) !!}
                                        </div>
                                        <div class="form-group">
                                            {!! Form::label('new_password', 'New Password', ['class' => 'control-label']) !!}
                                            {!! Form::password('new_password', ['class' => 'form-control']) !!}
                                        </div>
                                        <div class="form-group">
                                            {!! Form::label('new_password_confirmation', 'Confirm New Password', ['class' => 'control-label']) !!}
                                            {!! Form::password('new_password_confirmation', ['class' => 'form-control']) !!}
                                        </div>
                                    </div>
                                </div>
                                <a data-show-less-text='Hide Change Password' href="javascript:void(0);" class="in-form-link show-more-options">
                                    Change Password
                                </a>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="panel-footer">
                                            {!! Form::button('Cancel', ['class' => "btn modal-close btn-danger", 'data-dismiss' => 'modal']) !!}
                                            {!! Form::submit('Save Details', ['class' => 'btn btn-success pull-right']) !!}
                                        </div>
                                    </div>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

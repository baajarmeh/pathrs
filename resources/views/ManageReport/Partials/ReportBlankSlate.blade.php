@extends('Shared.Layouts.BlankSlate')

@section('blankslate-icon-class')
    ico-question2
@stop

@section('blankslate-title')
    No Reports Yet
@stop

@section('blankslate-text')
  New reports will appear here as they are created.
@stop

@section('blankslate-body')
    <button data-invoke="modal" data-modal-id='CreateReport' data-href="{{route('showCreateReport')}}" href='javascript:void(0);' class='btn btn-success mt5 btn-lg' type="button">
        <i class="ico-question"></i>
        Create Report
    </button>
@stop

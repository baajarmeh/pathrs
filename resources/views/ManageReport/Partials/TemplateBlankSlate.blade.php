@extends('Shared.Layouts.BlankSlate')

@section('blankslate-icon-class')
    ico-question2
@stop

@section('blankslate-title')
    No Tests Yet
@stop

@section('blankslate-text')
  New report templates will appear here as they are created.
@stop

@section('blankslate-body')
    <button data-invoke="modal" data-modal-id='CreateReportTemplate' data-href="{{route('showCreateReportTemplate')}}" href='javascript:void(0);'  class=' btn btn-success mt5 btn-lg' type="button" >
        <i class="ico-question"></i>
        Create Report Template
    </button>
@stop

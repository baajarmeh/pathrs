<div id="ReportForm" role="dialog" class="modal fade" style="display: none;">
    {!! Form::model($report, ['url' => route('postEditReport', ['report_id' => $report->id]), 'id' => 'edit-report-form', 'class' => 'ajax']) !!}

    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header text-center">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title">
                    <i class="ico-question"></i>
                    Edit Report
                </h3>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    {!! Form::label('patient_id', 'Patient', ['class' => 'control-label required']) !!}
                    {!! Form::select('patient_id', $patients, $report->patient_id, ['class' => 'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('name', 'Name', ['class' => 'control-label required']) !!}
                    {!! Form::text('name', $report->name, ['class' => 'form-control']) !!}
                </div>

                <div class="form-group">
                  {!! Form::label('doctor_ref', 'Doctor Reference', ['class' => 'control-label']) !!}
                  {!! Form::text('doctor_ref', $report->doctor_ref, ['class' => 'form-control']) !!}
                </div>

                <div class="form-group">
                  {!! Form::label('remarks', 'Remarks', ['class' => 'control-label']) !!}
                  {!! Form::textarea('remarks', $report->remark, ['class' => 'form-control editable', 'rows' => 5]) !!}
                </div>
            </div>
            <div class="modal-footer">
                {!! Form::button('Cancel', ['class' => "btn modal-close btn-danger", 'data-dismiss' => 'modal']) !!}
                {!! Form::submit('Save Report', ['class' => "btn btn-success"]) !!}
            </div>
        </div><!-- /end modal content-->
    </div>
    {!! Form::close() !!}
</div>

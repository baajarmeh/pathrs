@extends('Shared.Layouts.Master')

@section('title')
    @parent My Report
@stop

@section('top_nav')
@stop

@section('page_title')
    <i class='ico-clipboard4 mr5'></i>
    My Reports
@stop

@section('head')
@stop

@section('page_header')
    <div class="col-md-9 col-sm-6">
    </div>
    <div class="col-md-3 col-sm-6">
      {!! Form::open(array('url' => route('showPatientReports', ['sort_by' => $sort_by]), 'method' => 'get')) !!}
        <div class="input-group">
            <input name="q" value="{{$q or ''}}" placeholder="Search Reports.." type="text" class="form-control" />
            <span class="input-group-btn">
                <button class="btn btn-default" type="submit"><i class="ico-search"></i></button>
            </span>
        </div>
      {!! Form::close() !!}
    </div>
@stop

@section('content')
    <!--Start Reports table-->
    <div class="row">
        @if($reports->count())
            <div class="col-md-12">

                <!-- START panel -->
                <div class="panel">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <th>
                               Name
                            </th>
                            <th>
                               Created By
                            </th>
                            <th>
                               Result at
                            </th>
                            <th>
                               Doctor Ref.
                            </th>
                            <th>
                               Created at
                            </th>
                            <th></th>
                            </thead>

                            <tbody class="sortable">
                            @foreach ($reports as $report)
                                <tr id="report-{{ $report->id }}" data-report-id="{{ $report->id }}">
                                    <td>{{$report->name}}</td>
                                    <td>{{$report->owner->first_name}} {{$report->owner->last_name}}</td>
                                    <td>{{$report->result_at ? $report->result_at->format('d/m/Y H:i') : 'Not Yet'}}</td>
                                    <td>{{$report->doctor_ref}}</td>
                                    <td>{{$report->created_at->format('d/m/Y H:i')}}</td>
                                    <td class="text-center">
                                        <a class="btn btn-xs btn-primary loadModal" href="javascript:void(0);"
                                           data-href="{{route('postPatientDownloadReport', ['report_id' => $report->id])}}">
                                            Download
                                        </a>
                                        <a data-id="{{ $report->id }}"
                                           title="Send report to patient email direct."
                                           data-route="{{ route('postPatientEmailReport', ['report_id' => $report->id]) }}"
                                           data-type="question" href="javascript:void(0);"
                                           class="btn btn-xs btn-success">
                                            Send Email
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        @else
            @include('ManageReport.Partials.ReportBlankSlate')
        @endif
    </div>    <!--/End reports table-->
@stop

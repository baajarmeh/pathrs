<aside class="sidebar sidebar-left sidebar-menu">
    <section class="content">
        <h5 class="heading">Main Menu</h5>
        <ul id="nav_main" class="topmenu">
            <li class="{{ Request::is('*dashboard*') ? 'active' : '' }}">
                <a href="{{route('showDashboard')}}">
                    <span class="figure"><i class="ico-home2"></i></span>
                    <span class="text">Dashboard</span>
                </a>
            </li>
        </ul>
        <ul id="nav_event" class="topmenu">
            <li class="{{ Request::is('*reports*') ? 'active' : '' }}">
                <a href="{{route('showReports')}}">
                    <span class="figure"><i class="ico-user"></i></span>
                    <span class="text">Reports</span>
                </a>
            </li>
            <li class="{{ Request::is('*patients*') ? 'active' : '' }}">
                <a href="{{route('showPatients')}}">
                    <span class="figure"><i class="ico-bullhorn"></i></span>
                    <span class="text">Patients</span>
                </a>
            </li>
            <li class="{{ Request::is('*users*') ? 'active' : '' }}">
                <a href="{{route('showUsers')}}">
                    <span class="figure"><i class="ico-cog"></i></span>
                    <span class="text">Users</span>
                </a>
            </li>
        </ul>
    </section>
</aside>

<aside class="sidebar sidebar-left sidebar-menu">
    <section class="content">
        <h5 class="heading">Main Menu</h5>
        <ul id="nav_main" class="topmenu">
            <li class="{{ Request::is('*dashboard*') ? 'active' : '' }}">
                <a href="{{route('showPatientDashboard')}}">
                    <span class="figure"><i class="ico-home2"></i></span>
                    <span class="text">Dashboard</span>
                </a>
            </li>
        </ul>
        <ul id="nav_event" class="topmenu">
            <li class="{{ Request::is('*reports*') ? 'active' : '' }}">
                <a href="{{route('showPatientReports')}}">
                    <span class="figure"><i class="ico-user"></i></span>
                    <span class="text">Reports</span>
                </a>
            </li>
        </ul>
    </section>
</aside>

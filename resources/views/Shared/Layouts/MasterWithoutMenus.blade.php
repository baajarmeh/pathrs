<html>
    <head>
        <title>@yield('title')</title>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="{{ url("assets/images/touch/mstile-144x144.png") }}">
        <meta name="msapplication-config" content="{{ url("assets/images/touch/browserconfig.xml") }}">
        <meta name="application-name" content="PLRS">
        <meta name="_token" content="{{ csrf_token() }}" />

        @yield('head')

        <!--JS-->
       {!! HTML::script('vendor/jquery/dist/jquery.min.js') !!}
        <!--/JS-->

        <!--Style-->
       {!!HTML::style('assets/stylesheet/application.css')!!}
        <!--/Style-->

        <style>
            body {
                background: url({{asset('assets/images/background.png')}}) repeat;
                background-color: #2E3254;
            }
            h2 {
                text-align: center;
                margin-bottom: 31px;
                text-transform: uppercase;
                letter-spacing: 4px;
                font-size: 23px;
            }
            .panel {
                background-color: #ffffff;
                background-color: rgba(255,255,255,.95);
                padding: 15px 30px ;
                border: none;
                color: #333;
                box-shadow: 0 0 5px 0 rgba(0,0,0,.2);
                margin-top: 40px;
            }
            .panel a {
                color: #333;
                font-weight: 600;
            }
            .logo {
                text-align: center;
                margin-bottom: 20px;
            }
            .logo img {
                width: 200px;
            }
            .signup {
                margin-top: 10px;
            }
            .forgotPassword {
                font-size: 12px;
                color: #ccc;
            }
        </style>
    </head>
    <body>
        <section id="main" role="main">
            <section class="container">
                @yield('content')
            </section>
        </section>
        <div style="text-align: center; color: white" >
        </div>

        {!!HTML::script('assets/javascript/backend.js')!!}
    </body>
    @if(session()->get('message'))
        <script>showMessage('{{\Session::get('message')}}');</script>
    @endif
</html>
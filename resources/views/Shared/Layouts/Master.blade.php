<!DOCTYPE html>
<html>
<head>
    <title>
        @section('title')
            PLRS ::
        @show
    </title>

    <!--Meta-->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{ url("assets/images/touch/mstile-144x144.png") }}">
    <meta name="msapplication-config" content="{{ url("assets/images/touch/browserconfig.xml") }}">
    <meta name="application-name" content="PLRS">
    <meta name="_token" content="{{ csrf_token() }}" />
   <!--/Meta-->

    <!--JS-->
    {!! HTML::script('/vendor/jquery/dist/jquery.min.js') !!}
    <!--/JS-->

    <!--Style-->
    {!! HTML::style('/assets/stylesheet/application.css') !!}
    <!--/Style-->

    @yield('head')
</head>
<body class="plrs">
@yield('pre_header')
<header id="header" class="navbar">

    <div class="navbar-header">
        <a class="navbar-brand" href="javascript:void(0);">
            PLRS
        </a>
    </div>

    <div class="navbar-toolbar clearfix">
        @yield('top_nav')

        @if(!Auth::user()->id)
        <ul class="nav navbar-nav navbar-right">
            <li class="dropdown profile">
                <ul class="dropdown-menu" role="menu">
                    <li>
                        <a data-href="{{route('showEditProfile', ['user_id' => Auth::user()->id])}}" data-modal-id="EditProfile"
                           class="loadModal editProfileModal" href="javascript:void(0);">
                            <span class="icon ico-user"></span>My Profile
                        </a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="{{route('logout')}}"><span class="icon ico-exit"></span>Sign Out</a>
                    </li>
                </ul>
            </li>
        </ul>
        @endif
    </div>
</header>


@if(Auth::user()->type == 'patient')
    @include('Shared.Partials.PatientSidebar')
@else 
    @include('Shared.Partials.UserSidebar')
@endif

<!--Main Content-->
<section id="main" role="main">
    <div class="container-fluid">
        <div class="page-title">
            <h1 class="title">@yield('page_title')</h1>
        </div>
        @if(array_key_exists('page_header', View::getSections()))
        <!--  header -->
        <div class="page-header page-header-block row">
            <div class="row">
                @yield('page_header')
            </div>
        </div>
        <!--/  header -->
        @endif

        <!--Content-->
        @yield('content')
        <!--/Content-->
    </div>
    <!--To The Top-->
    <a href="#" style="display:none;" class="totop"><i class="ico-angle-up"></i></a>
    <!--/To The Top-->
</section>
<!--/Main Content-->

<!--JS-->
{!! HTML::script(asset('assets/javascript/backend.js')) !!}
<script>
    $(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-Token': "<?php echo csrf_token() ?>"
            }
        });
    });

    @if(!Auth::user()->first_name)
      setTimeout(function () {
        $('.editUserModal').click();
    }, 1000);
    @endif
</script>
<!--/JS-->
@yield('foot')

@if(session()->get('message'))
    <script>showMessage('{{\Session::get('message')}}');</script>
@endif
</body>
</html>
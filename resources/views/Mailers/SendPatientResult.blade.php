@extends('Emails.Layouts.Master')

@section('message_content')
Hello {{$patient->first_name}},<br><br>

Your result for the report <b>{{$report->name}}</b> is attached to this email.

<br><br>
Thank you
@stop

    Pathology Lab Reporting System
    ==============================

    What Is This?
    -------------

		This is Pathology Lab Reporting System based on PHP Laravel framwork.

    How To Use (Overview)
    ---------------------

      Patients:
			1. Patients can view their Reports online and login through http://localhost/plrs/public/patient-login with their 
			   email and passcode
			2. After log into system, patients can view all, download and send email their reports
	 
      Admins: ( Lab Technicians )
			1. Lab Technician (Admin) can manage their patients, tests, report result and their reports.
			2. Admin can login through http://localhost/plrs/public/login with their email and password.
			3. Patients section admin can add, edit and delete patient information. Patient passcode can be send as email by clicking send email link.
			4. For each patient, we can add multiple reports by choosing add Report in dropdown action menu.
			5. Reports can be added, edited and deleted as Text or Pdf file.
			6. Reports can be send as email by clicking email link.
			7. Tests section admin can add, edit and delete thier report.
	
    Pre-Requirements
	  -----------------
	
			1. PHP version 5.6 or newer is recommended.
			   (It should work on 5.2.4 as well, but we strongly advise you NOT to run such old versions of PHP, because of potential security and performance issues, as well as missing features.)
			2. MySQL (5.1+) via the mysql (deprecated), mysqli and pdo drivers.
			3. PHP OpenSSL extension for SwiftMailer SSL login.
			4. PDF plugin in browser to view PDF files. 
	
    How To Install The application
    ------------------------------

			1. Copy Source files into web server document directory ( Ex. project/src/ to /var/www/htdocs/path)
			2. Enter Project base url in config file. (path: /application/config/config.php   ex. $config['base_url'] = 'http://localhost/path';)
			3. Enter MySQL DB database credentials in database config file (path: /application/config/database.php)
			4. Import plrs-db.sql file in desired database
			5. Run composer for install dependancies project (php composer install)
			6. Enter Email configuration deatail in email confir file (path: /application/config/email.php)
			7. Optional SendGrid API configration can be done in helper file (path: /application/config/mail.php)
			8. Thats it! you can open patient report area like http://localhost/projectpath/patient/ and admin area http://localhost/projectpath/admin

    Feedback to Improve
	  -------------------
	
			1. Patient email and passcode authendication will not suits for all situation. There is chance to have Same name and same password. 
			   However i have handled to prevent dublicate But better to give unique email. 